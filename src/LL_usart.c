/*
 * LL_usart.c
 *
 *  Created on: 5 Nov 2018
 *      Author: NERD
 */


#include "LL_usart.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "string.h"

typedef struct {
     uint8_t buff[USART_MAX_BUFFER];
     uint8_t head;
     uint8_t tail;
     uint8_t size;
} ringBuffer_t;

ringBuffer_t rb[MAX_USART];

typedef struct pinUsart_t{
	USART_TypeDef *dev;
	GPIO_TypeDef *tx;
	GPIO_TypeDef *rx;
	uint16_t pinTx;
	uint16_t pinRx;
}pinUsart;


const pinUsart pinsUsart[]= {
	{ USART1, GPIOA, GPIOA, GPIO_Pin_9,  GPIO_Pin_10 },
	{ USART2, GPIOA, GPIOA, GPIO_Pin_2,  GPIO_Pin_3  },
	{ USART3, GPIOB, GPIOB, GPIO_Pin_10, GPIO_Pin_11 }
};

void initUsart(uint8_t dev, uint32_t baudrate, uint8_t config){
	GPIO_InitTypeDef gpio;
	USART_InitTypeDef usart;
	NVIC_InitTypeDef NVIC_InitStructure;

	memset(rb[dev].buff,0,sizeof(rb[dev].buff));
	rb[dev].head = 0;
	rb[dev].tail = 0;
	rb[dev].size = USART_MAX_BUFFER;

	if(dev == 0){
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1, ENABLE);
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA,ENABLE);
	} else if (dev == 1){
		RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2, ENABLE);
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA,ENABLE);
	} else {
		RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3, ENABLE);
		RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB,ENABLE);
	}

	gpio.GPIO_Mode		= GPIO_Mode_AF_PP;
	gpio.GPIO_Pin		= pinsUsart[dev].pinTx;
	gpio.GPIO_Speed		= GPIO_Speed_10MHz;
	GPIO_Init(pinsUsart[dev].tx,&gpio);

	gpio.GPIO_Mode		= GPIO_Mode_IPD;
	gpio.GPIO_Pin		= pinsUsart[dev].pinRx;
	gpio.GPIO_Speed		= GPIO_Speed_10MHz;
	GPIO_Init(pinsUsart[dev].rx,&gpio);

	usart.USART_BaudRate			= baudrate;
	usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart.USART_Mode				= USART_Mode_Rx | USART_Mode_Tx;
	usart.USART_Parity				= USART_Parity_No;
	usart.USART_StopBits			= USART_StopBits_1;
	usart.USART_WordLength			= USART_WordLength_8b;

	USART_Cmd(pinsUsart[dev].dev,ENABLE);
	USART_Init(pinsUsart[dev].dev,&usart);


	USART_ITConfig(pinsUsart[dev].dev, USART_IT_RXNE, ENABLE);
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn+dev;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	USART_ITConfig(pinsUsart[dev].dev,USART_IT_RXNE,ENABLE);
}

uint8_t writeRing(ringBuffer_t *f, uint8_t data){
    if(f->head + 1 == f->tail || (f->head +1 == f->size && f->tail == 0)){
        return 0;
    } else {
        f->buff[f->head] = data;
        f->head ++;
        if( f->head == f->size ){
            f->head = 0;
        }
        return 1;
    }
}

uint8_t wu(uint8_t data){
	USART_SendData(pinsUsart[0].dev,data);
	while(USART_GetFlagStatus(pinsUsart[0].dev, USART_FLAG_TXE) == RESET);
	return 0;
}

uint16_t getDataSize(ringBuffer_t *f){
    if(f->tail == f->head ){
        return 0;
    }
    if( f->tail > f->head ){
        return f->size - f->tail + f->head;
    }
    return f->head - f->tail;
}

uint8_t readRing(ringBuffer_t *f){
    uint8_t res;
    if(f->tail != f->head){
        res = f->buff [f->tail];
        f->tail++;
        if(f->tail == f->size)f->tail = 0;
        return res;
    }else{
        return 0;
    }
}

uint8_t usartDataAvailable(uint8_t dev){
	return (uint8_t)getDataSize(&rb[dev]);
}

uint8_t readUsart(uint8_t dev){
	return readRing(&rb[dev]);
}

uint8_t writeUsart(uint8_t dev, uint8_t data){
	USART_SendData(pinsUsart[dev].dev,data);
	while(USART_GetFlagStatus(pinsUsart[dev].dev, USART_FLAG_TXE) == RESET);
	return 0;
}

void USART1_IRQHandler(void){
	uint8_t data = 0;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET){
		data = USART_ReceiveData(USART1);
		if(!writeRing(&rb[0],data)){
			readRing(&rb[0]);
			(void)writeRing(&rb[0],data);
		}
	}
}

void USART2_IRQHandler(void){
	uint8_t data = 0;
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET){
		data = USART_ReceiveData(USART2);
		if(!writeRing(&rb[1],data)){
			readRing(&rb[1]);
			(void)writeRing(&rb[1],data);
		}
	}
}

void USART3_IRQHandler(void){
	uint8_t data = 0;
	if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET){
		data = USART_ReceiveData(USART3);
		if(!writeRing(&rb[2],data)){
			readRing(&rb[2]);
			(void)writeRing(&rb[2],data);
		}
	}
}

