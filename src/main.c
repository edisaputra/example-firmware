#include "stm32f10x.h"
#include "LL_usart.h"

uint8_t buffer[10];
static volatile uint8_t i = 0;

#define SERIAL1 0

void checkNewFirmware(){
	volatile uint16_t t = 10000;
	while(usartDataAvailable(SERIAL1)>0){
		buffer[i] = readUsart(SERIAL1);
		writeUsart(0,buffer[i]);
		i++;
		if(buffer[0] != 'B'){
			i=0;
		}
		if(i == (buffer[1] + 3)){
			if(buffer[i-1] != 'E'){
				i = 0;
			}else {
				if(buffer[1] == 0){
					writeUsart(SERIAL1,'O');
					BKP_WriteBackupRegister(BKP_DR1,0);
					BKP_WriteBackupRegister(BKP_DR2,0xFF);
					BKP_WriteBackupRegister(BKP_DR3,0x0A);
					while(--t);
					NVIC_SystemReset();
				}else{
					writeUsart(0,'a');
					i = 0;
				}
			}
		}
	}
}

int main(){
	volatile uint32_t t, temp;
	GPIO_InitTypeDef gpio;
	SystemInit();
	uint8_t data;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

	/* Enable write access to Backup domain */
	PWR_BackupAccessCmd(ENABLE);

	/* Clear Tamper pin Event(TE) pending flag */
	BKP_ClearFlag();


	initUsart(0,9600,0);

//	/* Check if the Power On Reset flag is set */
//	if( (RCC->CSR & (5<<26)) != 0) {
//	/* Clear reset flags */
//		RCC_ClearFlag();
//		data = BKP_ReadBackupRegister(BKP_DR1);
//		writeUsart(0,data);
//	}
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	gpio.GPIO_Pin = GPIO_Pin_13;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	GPIO_Init(GPIOC,&gpio);
	while(1){
		checkNewFirmware();
		writeUsart(0,'J');
		GPIOC->ODR ^= 1 << 13;
		t = 0x5FFFFF;
		while(t--);
	}
}
